<div style="margin-left:25%;padding:1px 16px;height:600px;">
  <h3>Candidatos Nacionales Registrados</h3>
  <div class="row">
			<div class="col-md-12">
				<div id="mapaTodos" style="height:500px; width:100%; border:2px solid black;">
				</div>
			</div>
		</div>
		<!-- Aquí va la función para inicializar el mapa con JS -->
			<script type="text/javascript">
				function initMap(){
					var centro=new google.maps.LatLng(-1.671471558607002,-78.67483618716183);
					var mapaTodosCandidatos=new google.maps.Map(
						document.getElementById('mapaTodos'),
						{
							center:centro,
							zoom: 7,
							mapTypeId:google.maps.MapTypeId.HYBRID
						}
					);
					// PHP imbuido para colocar un If y un FOReach para los marcadores
          <?php if($candidato): ?>
						<?php foreach ($candidato as $lugarTemporal): ?>
						var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_can;?>,
							<?php echo $lugarTemporal->longitud_can;?>);
						var marcador=new google.maps.Marker({
							position:coordenadaTemporal,
							title: "<?php echo $lugarTemporal->nombre_can;?>",
							map:mapaTodosCandidatos
						});
						<?php endforeach; ?>
					<?php endif; ?>
            google.maps.event.addListener(marcador,'click', function(){
              $('#miModal').modal('show');
            });
				}
			</script>
		<!-- Aquí acaba el JS -->
</div>
<!-- Modal -->
<div id="miModal" class="modal">
  <div class="modal-content">
    <h4>Modal de Ejemplo</h4>
    <p>Contenido del modal.</p>

  </div>
</div>
