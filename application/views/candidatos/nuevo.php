<div style="margin-left:25%;padding:1px 16px;height:640px;">
  <h1>Registro de un nuevo candidato</h1>
  <hr>
  <form class="" action="<?php echo site_url();?>/Candidatos/insertar" method="post">
  <!-- Aqui se define el tipo de método para el input, este es de tipo post -->
      <div class="row">
        <div id="myCarousel" class="col-md-6 carousel slide carousel-fade centered" data-ride="carousel" data-interval="2000" style="background-color:white;">
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img class="d-block w-100" src="<?php echo base_url();?>/assets/imgs/cand1.jpg" alt="Imagen 1">
            </div>
            <div class="item">
              <img class="d-block w-100" src="<?php echo base_url();?>/assets/imgs/cand2.png" alt="Imagen 2" width="480px">
            </div>
            <div class="item">
              <img class="d-block w-100" src="<?php echo base_url();?>/assets/imgs/cand3.png" alt="Imagen 3">
            </div>
          </div>
        </div>

        <div class="col-md-5">
            <!-- Nombre -->
            <label for="">Nombre:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el nombre del candidato"
            class="form-control"
            name="nombre_can" value=""
            id="nombre_can">
            <!-- Apellido -->
            <label for="">Apellido:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el apellido del candidato"
            class="form-control"
            name="apellido_can" value=""
            id="apellido_can">
            <!-- Movimiento -->
            <label for="">Movimiento:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el movimiento politico del candidato"
            class="form-control"
            name="movimiento_can" value=""
            id="movimiento_can">
            <!-- Dignidad -->
            <label for="">Dignidad:</label>
            <br>
            <select class="custom-select form-control" name="dignidad_can" id="dignidad_can">
              <option selected>Seleccione la dignidad del candidato</option>
              <option value="1">Presidente</option>
              <option value="2">Asambleista Nacional</option>
              <option value="3">Asambleista Provincial</option>
            </select>
            <!-- Latitud -->
            <label for="">Latitud:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la latitud de la ubicación del candidato"
            class="form-control"
            name="latitud_can" value=""
            id="latitud_can">
            <!-- Longitud -->
            <label for="">Longitud:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la longitud de la ubicación del candidato"
            class="form-control"
            name="longitud_can" value=""
            id="longitud_can">
            <!-- Propuesta -->
            <label for="exampleFormControlTextarea1">Propuesta del candidato:</label>
            <br>
            <textarea class="form-control"
             placeholder="Ingrese la propuesta del candidato"
             name="propuesta_can"
             id="propuesta_can"
             rows="3"></textarea>
            <!-- Ingreso de imagen -->
              <label class="form-label text-white m-1" for="foto_can">Escoger foto del candidato:</label>
              <input type="file" class="form-control d-none" name="foto_can" id="foto_can"/>
            <!-- Botones -->
            <br>
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url();?>/Candidatos/nuevo"class="btn btn-danger">Cancelar</a>
        </div>
      </div>
  </form>
</div>
