<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Politicos SIG DAG</title>
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <!-- bbostrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- api key google -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA--dJGtBylehwpMhSBpDlQZO097fMwewk&callback=initMap"></script>
  </head>
<!-- AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH -->
<!-- Aqui empieza el body -->
  <body>

    <style>
      ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 25%;
        background-color: #f1f1f1;
        position: fixed;
        height: 100%;
        overflow: auto;
      }

      li a {
        display: block;
        color: #000;
        padding: 8px 16px;
        text-decoration: none;
      }

      li a.active {
        background-color: #1257A4;
        color: white;
      }

      li a:hover:not(.active) {
        background-color: #555;
        color: white;
      }
    </style>

    <ul>
      <li class="text-center"><a class="active" href="<?php echo site_url('Welcome');?>"><img src="<?php echo base_url();?>/assets/imgs/logo.jpg" alt="" width=100%;></a></li>
      <li><a class="active glyphicon glyphicon-user" style="display:block" href="<?php echo site_url('Candidatos/nuevo');?>">  Registrar Candidato</a></li>
      <li><a class="glyphicon glyphicon-user" style="display:block" href="<?php echo site_url('Candidatos/todos');?>">  Candidatos Registrados</a></li>
      <li><a class="glyphicon glyphicon-user" style="display:block" href="<?php echo site_url('Candidatos/presidente');?>">  Candidatos Presidenciales</a></li>
      <li><a class="glyphicon glyphicon-user" style="display:block" href="<?php echo site_url('Candidatos/nacional');?>">  Candidatos Asambleistas Nacionales</a></li>
      <li><a class="glyphicon glyphicon-user" style="display:block" href="<?php echo site_url('Candidatos/provincial');?>">  Candidatos Asambleistas Provinciales</a></li>
      <li><p class="blockquote text-center"><br>Sitio web para el registro de candidatos políticos en distintos cargos, a nivel nacional.</p> </li>
      <li>
        <blockquote class="blockquote text-right">
          <p class="mb-0">El pueblo se merece a sus gobernantes.</p>
          <footer class="blockquote-footer">John Xina<cite title="Source Title"></cite></footer>
        </blockquote>
      </li>
    </ul>
