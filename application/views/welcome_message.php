<div style="margin-left:25%;padding:1px 16px;height:1000px;">
  <h2 class="text-center">Dándole el poder a la gente</h2>
  <h4 class="text-center">El sitio para el registro y consulta de candidatos políticos en el país al servicio del pueblo</h3>


		<style>
			.jumbotron {
			position: relative;
			overflow: hidden;
			background-color:black;
			}
			.jumbotron video {
			position: absolute;
			z-index: 1;
			top: 0;
			width:100%;
			height:100%;
			/*  object-fit is not supported on IE  */
			object-fit: cover;
			opacity:0.5;
			}
			.jumbotron .container {
			z-index: 2;
			position: relative;
			}
		</style>

		<script type="text/javascript">
			function deferVideo() {
				//defer html5 video loading
			$("video source").each(function() {
				var sourceFile = $(this).attr("data-src");
				$(this).attr("src", sourceFile);
				var video = this.parentElement;
				video.load();
				// uncomment if video is not autoplay
				//video.play();
			});
			}
			window.onload = deferVideo;
		</script>

		<div class="row">
			<div class="jumbotron jumbotron-fluid">
			<video autoplay muted loop poster="https://dummyimage.com/900x400/000/fff">
					<source src="" data-src="<?php echo base_url();?>/assets/vids/videoa360.mp4" type="video/mp4">
			</video>
				<div class="container text-white" style="color:white">
					<h1 class="display-4">Registra a tu candidato</h1>
					<p class="lead">Registra a quien creas que representa tus valores y principios de gobierno.</p>
					<hr class="my-4">
					<p>El futuro del país está en tus manos.</p>
				</div>
				<!-- /.container -->
			</div>
		</div>

		<div class="row">
			<div class="jumbotron jumbotron-fluid">
			<video autoplay muted loop poster="https://dummyimage.com/900x400/000/fff">
					<source src="" data-src="<?php echo base_url();?>/assets/vids/video2.mp4" type="video/mp4">
			</video>
				<div class="container text-white" style="color:white">
					<h1 class="display-4">El candidato para tu provincia</h1>
					<p>Que tu candidato sea el que mejor entienda las necesidades de tu localidad.</p>
					<hr class="my-4">
					<p class="lead">Registra a tu candidato de acuerdo a tu locación.</p>
				</div>
				<!-- /.container -->
			</div>
		</div>

		<div class="row">
			<div class="jumbotron jumbotron-fluid">
			<video autoplay muted loop poster="https://dummyimage.com/900x400/000/fff">
					<source src="" data-src="<?php echo base_url();?>/assets/vids/video3.mp4" type="video/mp4">
			</video>
				<div class="container text-white" style="color:white">
					<h1 class="display-4">Sé representado postulando al candidato ideal</h1>
					<p class="lead">Auf der Heide blüht ein kleines Blümelein.</p>
					<hr class="my-4">
					<p>und das heizt. ERIKA!</p>
				</div>
				<!-- /.container -->
			</div>
		</div>
</div>
