<?php
  class Candidatos extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      $this->load->model('Candidato');
    }
    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('candidatos/nuevo');
      $this->load->view('footer');
    }
    public function todos()
    {
      $data['candidato']=$this->Candidato->obtenerTodos();
      $this->load->view('header');
      $this->load->view('candidatos/todos',$data);
      $this->load->view('footer');
    }
    public function presidente()
    {
      $condiciones=array('dignidad_can'=>'1');
      $data['candidato']=$this->Candidato->obtenerPresidentes($condiciones);
      $this->load->view('header');
      $this->load->view('candidatos/presidente',$data);
      $this->load->view('footer');
    }
    public function nacional()
    {
      $condiciones=array('dignidad_can'=>'2');
      $data['candidato']=$this->Candidato->obtenerNacionales($condiciones);
      $this->load->view('header');
      $this->load->view('candidatos/nacional',$data);
      $this->load->view('footer');
    }
    public function provincial()
    {
      $condiciones=array('dignidad_can'=>'3');
      $data['candidato']=$this->Candidato->obtenerProvinciales($condiciones);
      $this->load->view('header');
      $this->load->view('candidatos/provincial',$data);
      $this->load->view('footer');
    }
    public function insertar(){
      $datosNuevoCandidato = array(
        'nombre_can' =>$this->input->post('nombre_can'),
        'apellido_can' =>$this->input->post('apellido_can'),
        'movimiento_can' =>$this->input->post('movimiento_can'),
        'dignidad_can' =>$this->input->post('dignidad_can'),
        'latitud_can' =>$this->input->post('latitud_can'),
        'longitud_can' =>$this->input->post('longitud_can'),
        'propuesta_can' =>$this->input->post('propuesta_can'),
        'foto_can' =>$this->input->post('foto_can')
    );
      if ($this->Candidato->insertar($datosNuevoCandidato)) {
        redirect('candidatos/todos');
      } else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }
  }
?>
