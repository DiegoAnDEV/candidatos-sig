<?php
  class Candidato extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function insertar($datosNuevoCandidato){
      return $this->db->insert('candidato',$datosNuevoCandidato);
    }
    public function obtenerTodos()
    {
      $listadoCandidatos=$this->db->get('candidato');
      if ($listadoCandidatos->num_rows()>0)
      {
        return $listadoCandidatos->result();
      } else {
        return false;
      }
    }
    public function obtenerPresidentes($condiciones)
    {
      $this->db->select('*');
      $this->db->from('candidato'); // Reemplaza 'tabla' con el nombre de tu tabla real
      $this->db->where($condiciones);
      $query = $this->db->get();
      return $query->result();
      // $listadoPresidentes=$this->db->get('candidato');
      // if ($listadoCandidatos->num_rows()>0)
      // {
      //   return $listadoCandidatos->result();
      // } else {
      //   return false;
      // }
    }
    public function obtenerNacionales($condiciones)
    {
      $this->db->select('*');
      $this->db->from('candidato'); // Reemplaza 'tabla' con el nombre de tu tabla real
      $this->db->where($condiciones);
      $query = $this->db->get();
      return $query->result();
    }
    public function obtenerProvinciales($condiciones)
    {
      $this->db->select('*');
      $this->db->from('candidato'); // Reemplaza 'tabla' con el nombre de tu tabla real
      $this->db->where($condiciones);
      $query = $this->db->get();
      return $query->result();
    }
  }
?>
